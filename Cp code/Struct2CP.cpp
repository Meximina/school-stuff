#include <iostream>
#include <string>
#include <bits/stdc++.h>

using namespace std;

struct Lista{
  
    int betyg;
    int rank;
    string namn;
    
};

bool Jämnför(Lista Spel, Lista SpelA)
{

    if (Spel.betyg < SpelA.betyg)
        return 0;
    else   
        return 1;    
 

}

void Rankning(Lista Spel[], int n){

    sort(Spel, Spel + n, Jämnför);
    
    for (int i = 0; i < n; i++)
        Spel[i].rank = i+1;
        
    
   
}
 
int main() {

int n = 3;

Lista Spel[n];



cout << "Här gör du en lista med dina favorit spel som du sedan kan ge ett betyg!\n";
cout << "Betygsätt med en siffra mellan 1 till 10.\n" << endl;

for(int i = 0; i<n; i++) {

    
    cout << "Spel namn: ";
    cin >> Spel[i].namn;
    cout << "Ge den ett betyg: ";
    cin >> Spel[i].betyg;

    
    }
     
Rankning(Spel, n);

cout << "Här är din lista med spel och deras betyg: \n";

for ( int i = 0; i < n; i++){
    
 
    cout << Spel[i].rank << ". " << Spel[i].namn << " Har betyget: " << Spel[i].betyg << endl;


    }   

}

/*
bool compareTwoStudents(Student a, Student b)
{
    // If total marks are not same then
    // returns true for higher total
    if (a.total != b.total)
        return a.total > b.total;
 
    // If marks in Maths are same then
    // returns true for higher marks
    if (a.math != b.math)
        return a.math > b.math;
 
    if (a.phy != b.phy)
        return a.phy > b.phy;
 
    return (a.che > b.che);
}

    sort(a, a + n, compareTwoStudents);
*/
