#include <string>
#include <iostream>
#include <thread>
#include <chrono>
#include <stdlib.h>
#include <time.h>
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <Windows.h>
#else
#include <unistd.h>
#endif
#include <cstdlib>

using namespace std;

#define color_black      0
#define color_dark_blue  1
#define color_dark_green 2
#define color_light_blue 3
#define color_dark_red   4
#define color_magenta    5
#define color_orange     6
#define color_light_gray 7
#define color_gray       8
#define color_blue       9
#define color_green     10
#define color_cyan      11
#define color_red       12
#define color_pink      13
#define color_yellow    14
#define color_white     15

string get_textcolor_code(const int textcolor) { // Linux only
    switch(textcolor) {
        case  0: return "30"; // color_black      0
        case  1: return "34"; // color_dark_blue  1
        case  2: return "32"; // color_dark_green 2
        case  3: return "36"; // color_light_blue 3
        case  4: return "31"; // color_dark_red   4
        case  5: return "35"; // color_magenta    5
        case  6: return "33"; // color_orange     6
        case  7: return "37"; // color_light_gray 7
        case  8: return "90"; // color_gray       8
        case  9: return "94"; // color_blue       9
        case 10: return "92"; // color_green     10
        case 11: return "96"; // color_cyan      11
        case 12: return "91"; // color_red       12
        case 13: return "95"; // color_pink      13
        case 14: return "93"; // color_yellow    14
        case 15: return "97"; // color_white     15
        default: return "37";
    }
}
string get_backgroundcolor_code(const int backgroundcolor) { // Linux only
    switch(backgroundcolor) {
        case  0: return  "40"; // color_black      0
        case  1: return  "44"; // color_dark_blue  1
        case  2: return  "42"; // color_dark_green 2
        case  3: return  "46"; // color_light_blue 3
        case  4: return  "41"; // color_dark_red   4
        case  5: return  "45"; // color_magenta    5
        case  6: return  "43"; // color_orange     6
        case  7: return  "47"; // color_light_gray 7
        case  8: return "100"; // color_gray       8
        case  9: return "104"; // color_blue       9
        case 10: return "102"; // color_green     10
        case 11: return "106"; // color_cyan      11
        case 12: return "101"; // color_red       12
        case 13: return "105"; // color_pink      13
        case 14: return "103"; // color_yellow    14
        case 15: return "107"; // color_white     15
        default: return  "40";
    }
}
string get_print_color(const int textcolor) { // Linux only
    return "\033["+get_textcolor_code(textcolor)+"m";
}
string get_print_color(const int textcolor, const int backgroundcolor) { // Linux only
    return "\033["+get_textcolor_code(textcolor)+";"+get_backgroundcolor_code(backgroundcolor)+"m";
}
void print_color(const int textcolor) {
#if defined(_WIN32)
    static const HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(handle, textcolor);
#elif defined(__linux__)
    cout << get_print_color(textcolor);
#endif // Windows/Linux
}
void print_color(const int textcolor, const int backgroundcolor) {
#if defined(_WIN32)
    static const HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(handle, backgroundcolor<<4|textcolor);
#elif defined(__linux__)
    cout << get_print_color(textcolor, backgroundcolor);
#endif // Windows/Linux
}
void print_color_reset() {
#if defined(_WIN32)
    static const HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(handle, 7); // reset color
#elif defined(__linux__)
    cout << "\033[0m"; // reset color
#endif // Windows/Linux
}

void println(const string& s="") {
    cout << s << endl;
}
void print(const string& s="") {
    cout << s;
}
void print(const string& s, const int textcolor) {
    print_color(textcolor);
    cout << s;
    print_color_reset();
}
void print(const string& s, const int textcolor, const int backgroundcolor) {
    print_color(textcolor, backgroundcolor);
    cout << s;
    print_color_reset();
}
void print_no_reset(const string& s, const int textcolor) { // print with color, but don't reset color afterwards (faster)
    print_color(textcolor);
    cout << s;
}
void print_no_reset(const string& s, const int textcolor, const int backgroundcolor) { // print with color, but don't reset color afterwards (faster)
    print_color(textcolor, backgroundcolor);
    cout << s;
}




void welcome(){

    print_no_reset("Welcome to slots! Here you can win big money if all the numbers are the same!", color_black, color_red | color_blue);
    cout << endl;


}

int main(){
    int money = 100;
    int bet = 0;
    int input = 0; 
    int num1 = 0;
    int num2 = 0;
    int num3 = 0;
    int odds = 4;
    int win = 0;
    bool game = true;
    bool loop = false;
    bool gamble = false;
    string cont1;
    string cont2;
    srand(time(NULL));
    
    while(game == true)
    {
    
       welcome();
        cout << "You have " << money << " dollars in total funds on your account.\n" << endl << "Do you wish to insert more?\n" << endl;
        cout << "yes/no?";
        bankInput:
        cout << endl;
        cin >> cont1; 
        cout << endl; 
        if(cont1 == "yes"){
            KeepOn:
            cout << "Insert amount:\n";
            cin >> input;
            money = money + input;
            cout << endl;
            cout << "You've inserted: " << input << " into your account and your total is " << money << endl;
            cout << endl;
            input = 0;
            gamble = true;
            } else if(cont1 == "no"){
                cout << "You have " << money << " in your bank.\n";
                cout << endl; 
                gamble = true;
            } else if(cont1 != "no" or "yes"){
                cout << "Plesure input yes or no.";
                goto bankInput;
            }
        while(gamble = true){cout << "How much do you want to bet? You have: " << money << " in your account\n" << endl;
            cin >> bet;
            cout << endl;
            money = money - bet;
            cout << "You've decided to bet " << bet << ". Lets hope you're luuuuckkkky!\n";
            cout << endl;
            num1 = rand()%3+1;
            num2 = rand()%3+1;
            num3 = rand()%3+1;
            cout.flush();
            sleep(2);
            cout << num1 << endl;
            sleep(1);
            cout << num2 << endl;
            sleep(1);
            if((num1==1 && num2 ==1) or 
            (num1==2 && num2 ==2) or 
            (num1==3 && num2 ==3)){
            
            std::string s="... ";
            for(char c: s){
                std::this_thread::sleep_for(std::chrono::seconds(1));
                std::cout << c << std::flush;
            } 
            cout << num3 << endl;
            } else { 
            cout << num3 << endl;
            }

            if((num1==1 && num2==1 && num3==1) or 
            (num1==2 && num2==2 && num3==2) or 
            (num1==3 && num2==3 && num3==3)){
            win = bet * odds;
            money = money+win;
            cout << "You win!\n";
            cout << endl;
            cout << win << " Has been added to your account! Woooh!\n";
            cout << endl;
            
            } else{
            cout << "You loose!\n";
            cout << endl;
            } if ( money  <= 0 ){
                EndScreen:
                cout << "You have ran out of money. Do you wish to input more?\n";
                cout << endl; 
                cout << "yes/no?\n";
                cin >> cont2;
                if(cont2 == "yes"){
                cout << endl;
                cout << "You've decided to put in more.\n";
                cout << endl;
                goto KeepOn;
                } else if (cont2 == "no"){
                cout << endl;
                cout << "Goodbye, have a good day.\n";
                return 0;
                } else if(cont2 != "yes" or "no"){
                    cout << "Plesure input yes or no.";
                goto EndScreen;
                
                }
            }
        }
    }
}
